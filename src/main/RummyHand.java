package main;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RummyHand {
	
	private final int NO_OF_SUITS = 5 ; 
	
	
	public ArrayList<ArrayList<Card>> splitIntoSuits(ArrayList<Card> hand) {
		
		ArrayList<ArrayList<Card> > suitWiseCards = new ArrayList<>(NO_OF_SUITS) ;
		for(int i = 0 ; i < hand.size() ; i++) {
			int idx = hand.get(i).suit.ordinal()  ; 
			ArrayList<Card> arr = suitWiseCards.get(idx) ; 
			arr.add(hand.get(i)) ; 
			suitWiseCards.set(idx, arr) ; 
		}
		for(int i = 0 ; i < NO_OF_SUITS ; i++) {
			Collections.sort(suitWiseCards.get(i), new Comparator<Card>() {
				public int compare(Card o1, Card o2) {
					return (o1.rank.ordinal() - o2.rank.ordinal()) ; 
					
				}
			});
		}
		return suitWiseCards ; 
	}
		
	public ArrayList<ArrayList<Card>> findingSequences(ArrayList<ArrayList<Card>> arr){
		
		for(int i=0; i<arr.size(); i++) {
			ArrayList<Card> temp = arr.get(i);
			for(int j=0; i<temp.size(); j++) {
				//if(temp))
			}
		}
		return arr;	
	}
	
	
	
	
	
	
	
	
}
