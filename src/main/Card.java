package main;

public class Card {
	Suit suit;
	Rank rank;
	
	public String getSuit() {
		return suit.toString();
	}
	
	public Rank rank(){
		return rank;
	}
	
	public Suit suit(){
		return suit;
	}

	public String getRank() {
		return rank.toString();
	}

	Card(Suit suit, Rank rank){
		this.rank = rank;
		this.suit = suit;
	}
	
	public String color(){
		switch(suit){
			case SPADE:
			case CLUB:
				return "Black";
			case HEART:
			case DIAMOND:
				return "Red";
		case JOKER:
			break;
		default:
			break;
		}
		return null;
	}
	
	public boolean isBlack(){
		return (color() == "Black")? true : false;
	}
	public boolean isRed(){
		return (color() == "Red")? true : false;
	}
	
	public boolean isJoker(){
		return (suit.toString() == "JOKER")? true : false;
	}
	
	public boolean equals(Card card){
		return (getRank() == card.getRank() && getSuit() == card.getSuit())? true : false;
	}
	
	public int compare(Card card){
		return rank.compareTo(card.rank());
	}
	
	public boolean isNext(Card card){
		return (rank.ordinal() + 1 == card.rank().ordinal())? true : false;
	}
	
	public boolean isPrev(Card card){
		return (rank.ordinal() -1 == card.rank().ordinal())? true : false;
	}
	public String toString(){
		return rank.toString() + " " + suit.toString();
	}
}
