package main;
public enum Rank {
	A(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), J(11), Q(12), K(13), JOKER(14);

	int priority;

	Rank(int p) {
		priority = p;
	}

	int showPriority() {
		return priority;
	}
}
