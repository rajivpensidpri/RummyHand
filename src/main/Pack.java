package main;
import java.util.ArrayList;

public class Pack {
	ArrayList<Card> cards54 = new ArrayList<Card>();	
	
	Pack(int jokerCount) {
		assemblePackCards();
		includeJoker(jokerCount);
	}

	private void includeJoker(int jokerCount) {
		while((jokerCount--) > 0) 
			cards54.add(new Card(Suit.JOKER,Rank.JOKER));
	}
	
	public ArrayList<Card> unPack() {
		return cards54;
	}
	
	public void assemblePackCards() {
        Card card;
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                if(!(rank.equals(Rank.JOKER)|| suit.equals(Suit.JOKER))) {
                		card = new Card(suit, rank);
                		cards54.add(card);
                }
            }
        }
    }
	
	
}
