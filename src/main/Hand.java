package main;

import java.util.*;

public class Hand implements Comparable<Hand> {
	private ArrayList<Card> hand;
	private int[] values;
	private int[] suits;
	
	public Hand(ArrayList<Card> hand)
	{
		this.hand = hand;
		
		values = values();
		suits = suits();
		
		Arrays.sort(values);
		Arrays.sort(suits);
	}
	
	private int[] values() {
        int[] result = new int[5];
        
        for (int i = 0; i < hand.size(); i++) {
            result[i] = hand.get(i).rank().showPriority();
        }
        
        return result;
	}
	
	private int[] suits() {
        int[] result = new int[5];
        
        for (int i = 0; i < hand.size(); i++) {
            result[i] = hand.get(i).suit().showSuit();
        }
        
        return result;
	}
	
	public double getRanking() {
        double ranking = 0;
        
        if (isStraightFlush() > 8.0) {
            ranking = isStraightFlush();
        } else if (isFourOfAKind() > 7.0) {
            ranking = isFourOfAKind();
        } else if (isFullHouse() > 6.0) {
            ranking = isFullHouse();
        } else if (isFlush() > 5.0) {
            ranking = isFlush();
        } else if (isStraight() > 4.0) {
            ranking = isStraight();
        } else if (isThreeOfAKind() > 3.0) {
            ranking = isThreeOfAKind();
        } else if (isTwoPair() > 2.0) {
            ranking = isTwoPair();
        } else if (isPair() > 1.0) {
            ranking = isPair();
        } else {
            
        }
        
        return ranking;
    }

	
	public double isPair() {
        double result = 0.0;
        
        for (int i = 0; i < values.length - 1; i++) {
            if (values[i] == values[i + 1]) {
                result = 1.0 + (values[i] * 0.01);
            }
        }
        
        return result;
	}
	
	public double isTwoPair() {
        double result = 0.0;
        double value = 0.0;
        int counter = 0; // Number of pairs.
        
        for (int i = 0; i < values.length - 1; i++) {
            if (values[i] == values[i + 1]) {
                counter++;
                
                value = values[i] * 0.01;
            }
        }
        
        if (counter == 2) {
            result = 2.0 + value;
        }
        
        return result;
	}
	
	public double isThreeOfAKind() {
        double result = 0.0;
        int counter = 0;
       
        for (int i = 0; i < values.length - 2; i++) {
            if (values[i] == values[i + 1] && values[i] == values[i + 2]) {
                result = 3.0 + (values[i] * 0.01);
            }
        }
        
        return result;
	}
	
	public double isStraight() {
        double result = 0.0;
        
        for (int i = 0; i < values.length - 1; i++){
            if (values[i] == values[i + 1] - 1) {
                result = 4.0 + (values[i + 1] * 0.01);
            } else {
                result = 0.0;
            }
        }
        
        return result;
	}
	
	public double isFlush() {
        double result = 0.0;
        
        int suit = suits[0];
        
        for (int i = 0; i < suits.length; i++) {
            result = 5.0 + (values[i] * 0.01);

            if (suits[i] != suit) {
                result = 0.0;
            }
        }
        
        return result;
	}
	
	public double isFullHouse() {
        double result = 0.0;
        boolean one = false;
        boolean two = false;
        
        for (int i = 0; i < values.length - 2; i++) {
            if (values[i] == values[i + 1] && values[i] == values[i + 2]) {
                one = true;
            }
        }
        
        if (values[3] == values[4]) {
            two = true;
        }
        
        if (one && two) {
            result = 6.0 + (values[values.length - 1] * 0.01);
        }
        
        return result;
	}
	
	public double isFourOfAKind() {
        double result = 0.0;
        int counter = 0;
       
        for (int i = 0; i < values.length - 3; i++) {
            if (values[i] == values[i + 1] && values[i] == values[i + 2] && values[i] == values[i + 3]) {
                result = 7.0 + (values[i] * 0.01);
            }
        }
        
        return result;
	}
	
	public double isStraightFlush() {
        double result = 0.0;
        
        if (isStraight() > 4.0 && isFlush() > 5.0) {
            result = 8.0 + isStraight() - 4.0;
        }
        
        return result;
	}
	
	public int compareTo(Hand other) {
        int result = 0;
        
        int thisIntRank = (int)this.getRanking();
        int otherIntRank = (int)other.getRanking();
        
        double tempThisHighCard = this.getRanking() - thisIntRank;
        double tempOtherHighCard = other.getRanking() - otherIntRank;
        
        int thisHighCard = (int)tempThisHighCard;
        int otherHighCard = (int)tempOtherHighCard;
        
        if (thisIntRank == otherIntRank){ 
            if (thisHighCard > otherHighCard) {
                result = 1;
            } else if (otherHighCard > thisHighCard) {
                result = -1;
            } else {
                result = 0;
            }
        } else if (thisIntRank > otherIntRank) { 
            result = 1;
        } else if (otherIntRank > thisIntRank) { 
            result = -1;
        } else { 
            result = 0;
        }
        
        return result;
	}
}
