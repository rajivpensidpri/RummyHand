package main;

public class Main {
    public static void main(String args[]){
        Deck d = new Deck(1,2);
        System.out.println(d);
        d.shuffle();
        System.out.println("Shuffled");
        System.out.println(d);
        System.out.println(d.deal(5));
        Deck d1 = new Deck(1,0);
        System.out.println(d1.size());
        System.out.println(d1.deal(5,2));
    }
}
