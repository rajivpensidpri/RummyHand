package main;

public enum Suit {
 	SPADE(1), CLUB(2), HEART(3), DIAMOND(4), JOKER(5);
	
	int suit;
	
	Suit(int s)
	{
		suit = s;
	}
	
	int showSuit() {
		return suit;
	}
}