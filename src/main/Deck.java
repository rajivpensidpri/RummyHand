package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class Deck {
    private ArrayList<Card> deck = new ArrayList<Card>();
    
    Deck(int numberOfPacks, int jokerPerPack) {
        for(int i=0; i < numberOfPacks; i++) {
            Pack p = new Pack(jokerPerPack);
            ArrayList<Card> cards= p.unPack();
            deck.addAll(cards);
        }
    }

    public ArrayList<Card> shuffle() {
        Collections.shuffle(deck);
        return deck;
    }

    public ArrayList<ArrayList<Card>> deal(int people){
        ArrayList<ArrayList<Card>> dealed = new ArrayList<ArrayList<Card>>();
        int i,j;
        for(j=0 ; j < people ; j++)
            dealed.add(new ArrayList<Card>());
        System.out.println(deck.size());
        int size = deck.size();
        for(j=0, i=0 ; i < size && j < people; j++, i++){
            dealed.get(j).add(deck.get(0));
            deck.remove(0);
            if((j + 1) == people)
                j = -1;
        }
        return dealed;
    }

    public ArrayList<ArrayList<Card>> deal(int people, int cardsPerPerson){
        ArrayList<ArrayList<Card>> dealed = new ArrayList<ArrayList<Card>>();
        int i,j;
        int totalCards = people * cardsPerPerson;
        int count = 0;
        for(j=0 ; j < people ; j++)
            dealed.add(new ArrayList<Card>());
        int deckSize = deck.size();
        for(j=0, i=0 ; j < people && i < deckSize && count < totalCards ; j++, i++, count++){
            dealed.get(j).add(deck.get(0));
            deck.remove(0);
            if((j + 1) == people)
                j=-1;
        }
        return dealed;
    }
    
    public Card drawTop(){
    		Card topCard = deck.get(0);
    		deck.remove(0);
    		return topCard;
    }

    public void addToBottom(ArrayList<Card> cards){
    		deck.addAll(cards);
    }
    
    public void addToBottom(Card card){
    		deck.add(card);
    }
    
    public void addToTop(ArrayList<Card> cards){
		deck.addAll(0,cards);
    }

    public void addToTop(Card card){
		deck.add(0,card);
    }
    
    public Card drawBottom(){
    		Card bottomCard = deck.get(deck.size()-1);
    		deck.remove(deck.size()-1);
    		return bottomCard;
    }

    public Card drawRandom() {
        int random = ThreadLocalRandom.current().nextInt(0, this.deck.size() + 1);
        Card randomCard = deck.get(random);
        deck.remove(random);
        return randomCard;
    }

    public String toString(){
        return deck.toString();
    }

    public int size(){
        return this.deck.size();
    }
}
